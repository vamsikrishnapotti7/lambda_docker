import json
import os

def handler(event, context):

    # TODO implementation

    for path, subdirs, files in os.walk(os.getcwd()):
        for name in files:
            print(os.path.join(path, name))

    ec2 = boto3.client('ec2')

    # Retrieves all regions/endpoints that work with EC2
    response = ec2.describe_regions()
    print('Regions:', response['Regions'])

    # Retrieves availability zones only for region of the ec2 object
    response = ec2.describe_availability_zones()
    print('Availability Zones:', response['AvailabilityZones'])

    return {
        'headers': {'Content-Type' : 'application/json'},
        'statusCode': 200,
        'body': json.dumps({"message": "Lambda Container image invoked!",
                            "event": event})
    }

# event = []

# context = []

# handler(event, context)

    # def load_config_file(account_alias, region):
    # print(account_alias, region)
    # files = [
    #     filename
    #     for filename in os.listdir("./configs")
    #     if os.path.splitext(filename)[0] in account_alias
    # ]
    # pp(files)

    # config = configparser.ConfigParser()
    # config.read_dict(json.load(open("./configs/" + files[0])))
    # for key in config[region]:
    #     print(key, config[region][key])
    #     response = set_ssm_parameter("/", key, "", config[region][key], tags)
    #     #/tsysfwk/account_name/network/vpc
    #     if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
    #         raise
