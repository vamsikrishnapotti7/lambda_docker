FROM public.ecr.aws/lambda/python:3.8
 
# Create function directory
WORKDIR /app

# Install the function's dependencies 
# Copy file requirements.txt from your project folder and install
# the requirements in the app directory. 

COPY requirements.txt  .
RUN  pip3 install -r requirements.txt

# Copy handler function (from the local app directory)
COPY  app.py  .

COPY configs/* ./configs/

# Overwrite the command by providing a different command directly in the template.
CMD ["/app/app.handler"]        
        


# # Define function directory
# ARG FUNCTION_DIR="/function"

# FROM python:buster as build-image

# # Install aws-lambda-cpp build dependencies
# RUN apt-get update && \
#   apt-get install -y \
#   g++ \
#   make \
#   cmake \
#   unzip \
#   libcurl4-openssl-dev

# # Include global arg in this stage of the build
# ARG FUNCTION_DIR
# # Create function directory
# RUN mkdir -p ${FUNCTION_DIR}

# # Copy function code
# COPY app/* ${FUNCTION_DIR}

# # Install the runtime interface client
# RUN pip install \
#         --target ${FUNCTION_DIR} \
#         awslambdaric

# # Multi-stage build: grab a fresh copy of the base image
# FROM python:buster

# # Include global arg in this stage of the build
# ARG FUNCTION_DIR
# # Set working directory to function root directory
# WORKDIR ${FUNCTION_DIR}

# # Copy in the build image dependencies
# COPY --from=build-image ${FUNCTION_DIR} ${FUNCTION_DIR}

# ENTRYPOINT [ "/usr/local/bin/python", "-m", "awslambdaric" ]
# CMD [ "app.handler" ]
        